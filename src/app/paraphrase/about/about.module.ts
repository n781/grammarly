import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './about.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [AboutComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [AboutComponent],
  entryComponents: [AboutComponent]
})
export class AboutModule { }
