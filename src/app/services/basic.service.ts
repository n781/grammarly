import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BasicService {

  constructor() { }

  public getSizeFrid() {
    return window.innerWidth < 568 ? 12 : 6;
  }
}
