import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(public http: HttpClient) { }

  ngOnInit() {

    const headers = new HttpHeaders();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    // eslint-disable-next-line @typescript-eslint/naming-convention
    const postData = { Text: 'i have a apple' };

    this.http.post('https://localhost:44366/translate/getText', postData, {headers})
      .subscribe(data => {
        console.log(data);
      }, error => {
        console.log(error);
      });
  }
}
