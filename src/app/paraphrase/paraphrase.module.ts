import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParaphraseRoutingModule } from './paraphrase-routing.module';
import { IonicModule } from '@ionic/angular';
import { ParaphraseComponent } from './paraphrase.component';
import { FormsModule } from '@angular/forms';
import { AboutModule } from './about/about.module';
import { BasicService } from '../services/basic.service';



@NgModule({
  declarations: [ParaphraseComponent],
  imports: [
    CommonModule,
    FormsModule,
    ParaphraseRoutingModule,
    IonicModule,
    AboutModule
  ],
  providers: [BasicService]
})
export class ParaphraseModule { }
