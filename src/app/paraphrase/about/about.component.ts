import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {

  @Input() sizeGrid = 6;
  abouts  = [
    {
      img: 'https://quillbot.com/images/landinPage/synonymNew.gif',
      class: '',
      isRight: false,
      header: 'CUSTOMIZATIONS',
      title: 'Vocabulary enhancements',
      description: 'Use the Synonyms to change more (or less) of your writing',
      bg: 'https://quillbot.com/images/landinPage/bluecercle.svg',
      position: '58% 165%'
    },
    {
      img: 'https://quillbot.com/images/landinPage/synonymImg.gif',
      class: 'content',
      isRight: true,
      header: 'AI-POWERED THESAURUS',
      title: 'Find the right synonym',
      description: 'Click on any word in your writing to find the right synonyms and get your perfect word!.',
      bg: 'https://quillbot.com/images/theme/light/paraphraser/greenCircle.svg',
      position: 'center 314px'
    },
    {
      img: 'https://quillbot.com/images/landinPage/intrgrationsNew.gif',
      class: '',
      isRight: false,
      header: 'AINTEGRATIONS',
      title: 'Plugs into the writing tools you already use',
      description: `QuillBot integrates directly into Google Docs and Chrome Extension. No more switching windows
       every time you want to Paraphrase text!`,
      bg: ''
    }
  ];
  constructor() { }

  ngOnInit() {
    console.log(this.sizeGrid);
  }

}
