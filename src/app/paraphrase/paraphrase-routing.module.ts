import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ParaphraseComponent } from './paraphrase.component';

const routes: Routes = [
  {
    path: '',
    component: ParaphraseComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParaphraseRoutingModule {}
