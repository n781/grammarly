/* eslint-disable @typescript-eslint/member-ordering */
import { Component, getPlatform, HostListener, OnInit } from '@angular/core';
import { MenuController, Platform } from '@ionic/angular';
import { BasicService } from '../services/basic.service';

@Component({
  selector: 'app-paraphrase',
  templateUrl: './paraphrase.component.html',
  styleUrls: ['./paraphrase.component.scss'],
})
export class ParaphraseComponent implements OnInit {

  modes = [
    {
      name: 'Standard',
      value: 'Standard'
    },
    {
      name: 'Fluency',
      value: 'Fluency'
    },
    {
      name: 'Creative',
      value: 'Creative'
    }, {
      name: 'Formal',
      value: 'Formal'
    }
    , {
      name: 'Shorten',
      value: 'Shorten'
    },
    {
      name: 'Expand',
      value: 'Expand'
    }
  ];
  modeSelected: string = this.modes[0].value;
  sizeGrid = 0;
  rowArea = 0;
  limit = 500;

  textParaphrase = '';
  resultParaphrase = '';
  constructor(private menu: MenuController, public basicService: BasicService) { }

  ngOnInit() {
    this.sizeGrid = this.basicService.getSizeFrid();
    this.rowArea = window.innerWidth < 568 ? 7 : 15;
   }

  openMMenu = () => {
    this.menu.open('menu-paraphrase');
  };

  onChangeMode = (event) => {
    console.log(event);
  };

}
